def m(n):
    print(n)
    def nb():
        print('******************')
        n()
        print('******************')
        # return n()
    return nb
@m
def ych():
    print('装饰器执行成功了！')

ych()


# 推导式
lst = [1,2,3,4,5,6,7,8]

new_lst = [i+j for i in lst if i%2 == 1 for j in lst if j%3 == 0]
print(new_lst)


#导入时间模块
import time
#装饰函数
def computer_runtime(func):
    #把run方法扩展功能之后的新方法
    def wrapper(*args,**kwargs):
        #函数开始时间
        start = time.time()
        print(start)
        #调用函数
        func(*args,**kwargs)
        #结束时间
        end = time.time()
        print(end)
        print('%s执行了%f 秒' %(func.__name__,end - start))
    #回调装饰函数
    return wrapper

@computer_runtime
#被装饰函数
def run(n,m = 9):
    # 休眠1秒
    time.sleep(1)
    #执行函数
    l = [x**2+n+m for x in range(1000)]

run(100)