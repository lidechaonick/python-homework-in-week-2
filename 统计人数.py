def a(self,z):
    print(self,z)
a(1,2)
#创建一个学生类
class Student(object):
    #类属性直接定义在类中
    count = 0
    def __init__(self,name):
        #实例对象属性定义在构造函数中
        self.name = name
        #每次创建一个对象，类属性的count+1
        Student.count += 1
        self.test('stu-%s'%(str(Student.count).zfill(3)))

    def test(self,stu):
        print('创建学生%s执行成功！'%(stu))
#查看类属性
print(Student)
#创建实例对象
# print(Student.count)
# xm = Student('小明')
# # print(id(Student))
# # print(id(Student('小明')))
# print(Student.count)
# xh = Student('小红')
# # print(id(Student))
# # print(id(Student('小红')))
# print(Student.count)
# xw = Student('小王')
# # print(id(Student))
# # print(id(Student('小王')))
# print(Student.count)
# xx = Student('小新')
# # print(id(Student))
# # print(id(Student('小新')))
# print(Student.count)
#
# xm.test()
#通过创建对象来访问对象属性
# print(xm.name)
# print(xx.count)

i = 1
while i<=10:
    print(Student('学生姓名：%s'%('stu-'+str(i).zfill(3))).name)
    print('学生序号：%d :'%(Student.count))
    i += 1
